# PHP AST dumper #

This is a simple PHP script which can be used to dump the abstract syntax tree (AST) of a PHP program specified on the command line

## Usage ##

```
    php_ast /path/to/example.php
```

The script will read ````example.php```` and dump the AST on standard output.

## Installation ##

This assumes that you have [Composer](https://getcomposer.org/) installed.

* Clone the git repository:


```
#!sh
    % git clone git@bitbucket.org:ixm/php_ast.git
```

* Install the dependencies:

```
#!sh
    % cd php_ast
    % composer install
```

## Example application ##

### Clean up code to coding standards ###

We'd like to apply some formatting/layout changes to code to
comply with coding standards. We want to be sure that these
changes do not affect the behaviour of the program. Tools
like ````diff```` show many trivial changes making it difficult to
see what substantive changes occurred.

Approach: Dump the AST of the code before and after the
transformations, and compare the ASTs. If there are no
differences, the behavior of the code will be unchanged.

Because the AST ignores whitespace and canonicalizes many aspects of
the code, trivial code changes don't show up as changes in the AST.

```
#!sh
    % php_ast example.php > example.ast.before
    % vi example.php 
    % php_ast example.php > example.ast.after
    % sdiff --ignore-all-space -B -s example.ast.before example.ast.after
```


## Who do I talk to? ##

* djun at imagexmedia.com
